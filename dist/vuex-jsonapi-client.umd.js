(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('json-api-normalizer'), require('vue'), require('axios'), require('vue-resource'), require('p-queue')) :
  typeof define === 'function' && define.amd ? define(['exports', 'json-api-normalizer', 'vue', 'axios', 'vue-resource', 'p-queue'], factory) :
  (factory((global.VuexJsonapiClient = {}),global.normalize,global.Vue,global.axios,global.VueResource,global.PQueue));
}(this, (function (exports,normalize,Vue,axios,VueResource,PQueue) { 'use strict';

  normalize = normalize && normalize.hasOwnProperty('default') ? normalize['default'] : normalize;
  Vue = Vue && Vue.hasOwnProperty('default') ? Vue['default'] : Vue;
  axios = axios && axios.hasOwnProperty('default') ? axios['default'] : axios;
  VueResource = VueResource && VueResource.hasOwnProperty('default') ? VueResource['default'] : VueResource;
  PQueue = PQueue && PQueue.hasOwnProperty('default') ? PQueue['default'] : PQueue;

  var get = function (state, ref) {
    if (!ref) { return }
    if (!state[ref.type]) { return }
    return state[ref.type][ref.id]
  };

  var getCollection = function (state, type) {
    if (!state[type]) { return }
    return Object.values(state[type])
  };

  var entryToRef = function (entry) {
    if (!entry) { return entry }
    return { id: entry.id, type: entry.type }
  };

  var sameRef = function (entry1, entry2) {
    return entry1 && entry2 &&
      entry1.type === entry2.type &&
      entry1.id === entry2.id
  };

  var entryArrayToRef = function (entryArray) {
    if (!entryArray) { return entryArray }
    return entryArray.map(function (entry) { return entryToRef(entry); })
  };

  var attribute = function (entry, name) {
    if (!entry) { return }
    if (!entry.attributes) { return }
    return entry.attributes[name]
  };

  var relationship = function (entry, name) {
    if (!entry) { return }
    if (!entry.relationships) { return }
    if (!entry.relationships[name]) { return }
    return entry.relationships[name].data
  };

  var utils = /*#__PURE__*/Object.freeze({
    get: get,
    getCollection: getCollection,
    entryToRef: entryToRef,
    sameRef: sameRef,
    entryArrayToRef: entryArrayToRef,
    attribute: attribute,
    relationship: relationship
  });

  var defaultExport = function defaultExport (ref) {
    var state = ref.state;
    var payload = ref.payload;

    this.state = state;
    this.payload = payload;
  };
  defaultExport.prototype.perform = function perform () {
    this._init();
    this._add();
  };

  defaultExport.prototype._init = function _init () {
    if (this.state[this._type()]) { return }
    this._vue().set(this.state, this._type(), {});
  };
  defaultExport.prototype._add = function _add () {
    if (this._isPresent()) { return }

    this._vue().set(this._collection(), this.payload.id, this.payload);
  };

  defaultExport.prototype._type = function _type () {
    return this.payload.type
  };
  defaultExport.prototype._collection = function _collection () {
    return this.state[this._type()]
  };

  defaultExport.prototype._isPresent = function _isPresent () {
    return get(this.state, {
      type: this._type(),
      id: this.payload.id
    }) !== undefined
  };
  defaultExport.prototype._vue = function _vue () {
    return this.state.vue
  };

  var defaultExport$1 = function defaultExport (ref) {
    var state = ref.state;
    var payload = ref.payload;
    var resource = ref.resource;

    this.state = state;
    this.payload = payload;
    this.resource = resource;
  };
  defaultExport$1.prototype.perform = function perform () {
    this._addCollections();
    this._addMetaResource();
  };

  defaultExport$1.prototype._addCollections = function _addCollections () {
    for (var i = 0, list = Object.keys(this._normalizedPayload()); i < list.length; i += 1) {
      var key = list[i];

        if (key !== 'meta') { this._addCollection(key); }
    }
  };
  defaultExport$1.prototype._addMetaResource = function _addMetaResource () {
    this._init(this.state, 'meta', { });
    var metaData = this._normalizedPayload().meta;

    if (!metaData) { return }

    for (var i$1 = 0, list$1 = Object.keys(metaData); i$1 < list$1.length; i$1 += 1) {
      var resource = list$1[i$1];

        var data = null;
      if (Array.isArray(this.payload.data)) {
        data = entryArrayToRef(metaData[resource].data);
        for (var i = 0, list = data; i < list.length; i += 1) {
          var entry = list[i];

            this._vue().set(this.state.meta, (resource + "/" + (entry.id)), { data: entry });
        }
      } else {
        data = entryToRef(metaData[resource].data[0]);
      }
      this._vue().set(this.state.meta, resource, { data: data, info: this._info() });
    }
  };

  defaultExport$1.prototype._addCollection = function _addCollection (key) {
    var source = this._normalizedPayload();

    this._init(this.state, key, {});
    for (var i = 0, list = Object.entries(source[key]); i < list.length; i += 1) {
      var ref = list[i];
        var entryId = ref[0];
        var entry = ref[1];

        this._vue().set(this.state[key], entryId, entry);
    }
  };
  defaultExport$1.prototype._init = function _init (state, key, value) {
    if (state[key]) { return }
    this._vue().set(state, key, value);
  };

  defaultExport$1.prototype._normalizedPayload = function _normalizedPayload () {
    if (!this._normalizedPayloadVar) {
      this._normalizePayloadVar =
        normalize(this.payload, {
          endpoint: this.resource,
          camelizeTypeValues: false,
          camelizeKeys: false
        });
    }

    return this._normalizePayloadVar
  };
  defaultExport$1.prototype._info = function _info () {
    var queryString = this.resource.replace(new RegExp('^[^?]*.'), '');
    var keyValuePairs = queryString.split('&');
    var result = {};

    if (!queryString) { return }

    for (var i = 0, list = keyValuePairs; i < list.length; i += 1) {
      var keyValuePair = list[i];

        var key = keyValuePair.split('=')[0];
      var value = keyValuePair.split('=')[1];
      var match = key.match(/([a-zA-Z0-9]*)\[([a-zA-Z0-9_]*)\]/);

      if (match.length === 3) {
        if (match[2] === '') {
          if (!result[match[1]]) { result[match[1]] = []; }
          result[match[1]].push(value);
        } else {
          result[match[1]] = value.split(',');
        }
      } else {
        result[key] = value;
      }
    }
    return result
  };
  defaultExport$1.prototype._vue = function _vue () {
    return this.state.vue
  };

  var defaultExport$2 = function defaultExport (ref) {
    var vue = ref.vue;
    var entry = ref.entry;
    var payload = ref.payload;

    this.vue = vue;
    this.entry = entry;
    this.payload = payload;
  };
  defaultExport$2.prototype.perform = function perform () {
    if (this.payload.attributes) { this._updateAttributes(); }
    if (this.payload.relationships) { this._updateRelationships(); }
  };

  defaultExport$2.prototype._updateAttributes = function _updateAttributes () {
    if (!this.entry.attributes) { vue.set(this.entry, 'attributes', {}); }

    var attributes = this.entry.attributes;

    for (var i = 0, list = Object.keys(this.payload.attributes); i < list.length; i += 1) {
      var key = list[i];

        this.vue.set(attributes, key, this.payload.attributes[key]);
    }
  };
  defaultExport$2.prototype._updateRelationships = function _updateRelationships () {
    if (!this.entry.relationships) { this.entry.relationships = []; }

    var relationships = this.entry.relationships;

    for (var i = 0, list = Object.keys(this.payload.relationships); i < list.length; i += 1) {
      var key = list[i];

        relationships[key] = this.payload.relationships[key];
    }
  };

  var defaultExport$3 = function defaultExport (ref) {
    var state = ref.state;
    var entry = ref.entry;

    this.state = state;
    this.entry = entry;
  };
  defaultExport$3.prototype.perform = function perform () {
    this._removeFromCollection();
    this._removeFromMeta();
  };

  defaultExport$3.prototype._removeFromCollection = function _removeFromCollection () {
    this._vue().delete(this._collection(), this.entry.id);
  };
  defaultExport$3.prototype._removeFromMeta = function _removeFromMeta () {
    for (var i = 0, list = Object.entries(this.state.meta); i < list.length; i += 1) {
      var ref = list[i];
        var resource = ref[0];
        var data = ref[1];

        if (Array.isArray(data)) {
        this._vue().set(this.state.meta, resource, this._filterMetaCollection(data));
      }
      if (this._shouldDelete(data)) { this._vue().delete(this.state.meta, resource); }
    }
  };

  defaultExport$3.prototype._collection = function _collection () {
    return this.state[this.entry.type]
  };
  defaultExport$3.prototype._filterMetaCollection = function _filterMetaCollection (data) {
      var this$1 = this;

    return data.filter(function (entry) { return !sameRef(entry, this$1.entry); })
  };
  defaultExport$3.prototype._shouldDelete = function _shouldDelete (data) {
    return data.length === 0 || sameRef(data, this.entry)
  };
  defaultExport$3.prototype._vue = function _vue () {
    return this.state.vue
  };

  var defaultExport$4 = function defaultExport (ref) {
    var state = ref.state;
    var child = ref.child;
    var parentType = ref.parentType;
    var parentRelationshipName = ref.parentRelationshipName;

    this.state = state;
    this.child = child;
    this.parentType = parentType;
    this.parentRelationshipName = parentRelationshipName;
  };
  defaultExport$4.prototype.perform = function perform () {
    for (var i = 0, list = Object.values(this._collection()); i < list.length; i += 1) {
      var entry = list[i];

        if (!this._hasRelationship(entry)) { continue }
      this._relationship(entry).data.splice(this._index(entry));
    }
  };

  defaultExport$4.prototype._collection = function _collection () {
    return this.state[this.parentType]
  };
  defaultExport$4.prototype._hasRelationship = function _hasRelationship (entry) {
    if (!entry.relationships) { return false }
    if (!this._relationship(entry)) { return false }
    if (!this._relationship(entry).data) { return false }

    return true
  };
  defaultExport$4.prototype._relationship = function _relationship (entry) {
    return entry.relationships[this.parentRelationshipName]
  };
  defaultExport$4.prototype._index = function _index (entry) {
    return this._relationship(entry).data.findIndex(function (localEntry) {
      return sameRef(entry, localEntry)
    })
  };

  var defaultExport$5 = function defaultExport (ref) {
    var parent = ref.parent;
    var child = ref.child;
    var relationshipName = ref.relationshipName;

    this.parent = parent;
    this.child = child;
    this.relationshipName = relationshipName;
  };
  defaultExport$5.prototype.perform = function perform () {
    this._initialize();
    this._addToRelationship();
  };

  defaultExport$5.prototype._initialize = function _initialize () {
    if (!this.parent.relationships) { this.parent.relationships = {}; }
    if (!this._relationship()) { this.parent.relationships[this.relationshipName] = {}; }
    if (!this._relationshipData()) { this._relationship().data = []; }
  };
  defaultExport$5.prototype._addToRelationship = function _addToRelationship () {
    this._relationshipData().push(this._associatedEntry());
  };

  defaultExport$5.prototype._relationship = function _relationship () {
    return this.parent.relationships[this.relationshipName]
  };
  defaultExport$5.prototype._relationshipData = function _relationshipData () {
    return this._relationship().data
  };
  defaultExport$5.prototype._associatedEntry = function _associatedEntry () {
    return { id: this.child.id, type: this.child.type }
  };

  var defaultExport$6 = function defaultExport (ref) {
    var parent = ref.parent;
    var child = ref.child;
    var relationshipName = ref.relationshipName;

    this.parent = parent;
    this.child = child;
    this.relationshipName = relationshipName;
  };
  defaultExport$6.prototype.perform = function perform () {
    this._initialize();
    this._setRelationship();
  };

  defaultExport$6.prototype._initialize = function _initialize () {
    if (!this._relationships()) { this.child.relationships = {}; }
    if (!this._relationship()) { this._relationships()[this.relationshipName] = {}; }
  };
  defaultExport$6.prototype._setRelationship = function _setRelationship () {
    this._relationship().data = this._relationshipEntry();
  };

  defaultExport$6.prototype._relationships = function _relationships () {
    return this.child.relationships
  };
  defaultExport$6.prototype._relationship = function _relationship () {
    return this._relationships()[this.relationshipName]
  };
  defaultExport$6.prototype._relationshipEntry = function _relationshipEntry () {
    if (!this.parent) { return null }
    return { id: this.parent.id, type: this.parent.type }
  };

  var defaultExport$7 = function defaultExport (ref) {
    var state = ref.state;
    var children = ref.children;
    var parent = ref.parent;
    var parentTypes = ref.parentTypes;
    var parentRelationshipName = ref.parentRelationshipName;
    var childRelationshipName = ref.childRelationshipName;

    this.state = state;
    this.children = children;
    this.parent = parent;
    this.parentTypes = parentTypes;
    this.parentRelationshipName = parentRelationshipName;
    this.childRelationshipName = childRelationshipName;
  };
  defaultExport$7.prototype.perform = function perform () {
    this._clear();
    this._setOnParent();
    this._setOnChildren();
  };

  defaultExport$7.prototype._clear = function _clear () {
    for (var i$1 = 0, list$1 = this.parentTypes; i$1 < list$1.length; i$1 += 1) {
      var type = list$1[i$1];

        for (var i = 0, list = this.children; i < list.length; i += 1) {
        var child = list[i];

          this._clearFromCollection(this.state, child, type,
          this.parentRelationshipName);
      }
    }
  };
  defaultExport$7.prototype._setOnParent = function _setOnParent () {
    this._initRelationshipCollection(this.parent,
      this.parentRelationshipName);

    for (var i = 0, list = this.children; i < list.length; i += 1) {
      var child = list[i];

        this.parent.relationships[this.parentRelationshipName].data
        .push(entryToRef(child));
    }
  };
  defaultExport$7.prototype._setOnChildren = function _setOnChildren () {
    for (var i = 0, list = this.children; i < list.length; i += 1) {
      var child = list[i];

        this._setRelationshipEntry(
        child,
        this.childRelationshipName,
        entryToRef(this.parent)
      );
    }
  };
  defaultExport$7.prototype._clearFromCollection = function _clearFromCollection (state, entry, type, relationshipName) {
    for (var i = 0, list = getCollection(state, type); i < list.length; i += 1) {
      var collectionEntry = list[i];

        if (!collectionEntry.relationships) { continue }
      if (!collectionEntry.relationships[relationshipName]) { continue }

      var relationship$$1 = collectionEntry.relationships[relationshipName].data;
      var index = relationship$$1.findIndex(function (refEntry) {
        return refEntry.id === entry.id && refEntry.type === entry.type
      });
      if (index > -1) { relationship$$1.splice(index, 1); }
    }
  };
  defaultExport$7.prototype._initRelationshipCollection = function _initRelationshipCollection (entry, relationshipName) {
    if (!entry.relationships) { this._vue().set(entry, 'relationships', {}); }
    if (!entry.relationships[relationshipName]) {
      this._vue().set(entry.relationships, relationshipName, {});
    }
    if (!entry.relationships[relationshipName].data) {
      this._vue().set(entry.relationships[relationshipName], 'data', []);
    }
  };
  defaultExport$7.prototype._setRelationshipEntry = function _setRelationshipEntry (entry, relationshipName, associatedEntry) {
    if (!entry.relationships) { this._vue().set(entry, 'relationships', {}); }
    if (!entry.relationships[relationshipName]) {
      this._vue().set(entry.relationships, relationshipName, {});
    }
    if (!entry.relationships[relationshipName].data) {
      this._vue().set(entry.relationships[relationshipName], 'data', associatedEntry);
    }
  };
  defaultExport$7.prototype._vue = function _vue () {
    return this.state.vue
  };

  var defaultExport$8 = function defaultExport (ref) {
    var state = ref.state;
    var exclude = ref.exclude;

    this.state = state;
    this.exclude = exclude;
  };
  defaultExport$8.prototype.perform = function perform () {
    for (var i = 0, list = Object.keys(this.state); i < list.length; i += 1) {
      var key = list[i];

        if (key === 'config') { continue }
      this._vue().delete(this.state, key);
    }
  };

  defaultExport$8.prototype._vue = function _vue () {
    return this.state.vue
  };

  var defaultExport$9 = function defaultExport (ref) {
    var vue = ref.vue;
    var entry = ref.entry;
    var association = ref.association;
    var response = ref.response;

    this.vue = vue;
    this.entry = entry;
    this.association = association;
    this.response = response;
  };
  defaultExport$9.prototype.perform = function perform () {
    this._addData();
    this._setLink();
  };

  defaultExport$9.prototype._addData = function _addData () {
      var this$1 = this;

    this.response.data.forEach(function (item) {
      this$1._relationship().data.push(item);
    });
  };
  defaultExport$9.prototype._setLink = function _setLink () {
    this.vue.set(this._relationship(), 'links', this.response.links);
  };

  defaultExport$9.prototype._relationship = function _relationship () {
    return this.entry.relationships[this.association]
  };

  var defaultExport$a = function defaultExport (ref) {
    var vue = ref.vue;
    var entry = ref.entry;
    var association = ref.association;
    var links = ref.links;

    this.vue = vue;
    this.entry = entry;
    this.association = association;
    this.links = links;
  };
  defaultExport$a.prototype.perform = function perform () {
    this.vue.set(this._relationship(), 'links', this.links);
  };

  defaultExport$a.prototype._relationship = function _relationship () {
    return this.entry.relationships[this.association]
  };

  var Mutations = {
    add: function add (state, payload) {
      new defaultExport({ state: state, payload: payload }).perform();
    },
    update: function update (state, ref) {
      var entry = ref.entry;
      var payload = ref.payload;

      new defaultExport$2({ vue: state.vue, entry: entry, payload: payload }).perform();
    },
    remove: function remove (state, entry) {
      new defaultExport$3({ state: state, entry: entry }).perform();
    },
    removeFromAll: function removeFromAll (state, ref) {
      var child = ref.child;
      var parentType = ref.parentType;
      var parentRelationshipName = ref.parentRelationshipName;

      new defaultExport$4({ state: state, child: child, parentType: parentType, parentRelationshipName: parentRelationshipName }).perform();
    },
    addToMultiple: function addToMultiple (state, ref) {
      var parent = ref.parent;
      var child = ref.child;
      var relationshipName = ref.relationshipName;

      new defaultExport$5({ parent: parent, child: child, relationshipName: relationshipName }).perform();
    },
    setAssociation: function setAssociation (state, ref) {
      var child = ref.child;
      var parent = ref.parent;
      var relationshipName = ref.relationshipName;

      new defaultExport$6({ parent: parent, child: child, relationshipName: relationshipName }).perform();
    },
    relationship: function relationship$$1 (state, ref) {
      var obj;

      var entry = ref.entry;
      var relationship$$1 = ref.relationship;
      var data = ref.data;
      new defaultExport$2({
        vue: state.vue,
        entry: entry,
        payload: {
          relationships: ( obj = {}, obj[relationship$$1] = {
              data: data
            }, obj )
        }
      }).perform();
    },
    clear: function clear (state, ref) {
      var exclude = ref.exclude;

      new defaultExport$8({ exclude: exclude }).perform();
    },
    clearCalledUrls: function clearCalledUrls (state) {
      if (!state['called-urls']) { return }
      state.vue.delete(state, 'called-urls');
    },
    addCalledUrl: function addCalledUrl (state, ref) {
      var url = ref.url;
      var promise = ref.promise;

      if (!state['called-urls']) { state.vue.set(state, 'called-urls', {}); }
      state.vue.set(state['called-urls'], url, promise);
    },
    changeManyToOneReference: function changeManyToOneReference (state, ref) {
      var children = ref.children;
      var parent = ref.parent;
      var parentTypes = ref.parentTypes;
      var parentRelationshipName = ref.parentRelationshipName;
      var childRelationshipName = ref.childRelationshipName;

      new defaultExport$7({
        state: state,
        children: children,
        parent: parent,
        parentTypes: parentTypes,
        parentRelationshipName: parentRelationshipName,
        childRelationshipName: childRelationshipName
      }).perform();
    },
    addByNormalize: function addByNormalize (state, ref) {
      var payload = ref.payload;
      var resource = ref.resource;

      new defaultExport$1({ state: state, payload: payload, resource: resource }).perform();
    },
    changeMetaCollection: function changeMetaCollection (state, ref) {
      var name = ref.name;
      var collection = ref.collection;

      state.meta[name].data = entryArrayToRef(collection);
    },
    setEndpoint: function setEndpoint (state, endpoint) {
      state.endpoint = endpoint;
    },
    addPageToAssociation: function addPageToAssociation (state, ref) {
      var entry = ref.entry;
      var association = ref.association;
      var response = ref.response;

      new defaultExport$9({ vue: state.vue, entry: entry, association: association, response: response }).perform();
    },
    relataionshipLinks: function relataionshipLinks (state, ref) {
      var entry = ref.entry;
      var association = ref.association;
      var links = ref.links;

      new defaultExport$a({ vue: state.vue, entry: entry,  association: association, links: links }).perform();
    },
    headerValue: function headerValue (state, ref) {
      var key = ref.key;
      var value = ref.value;

      Vue.http.headers.common[key] = value;
    },
    vue: function vue (state, vue$1) {
      state.vue = vue$1;
    },
    errorHandler: function errorHandler (state, handler) {
      state.errorHandler = handler;
    }
  };

  var defaultExport$b = function defaultExport (ref) {
    var state = ref.state;
    var name = ref.name;

    this.state = state;
    this.name = name;
  };
  defaultExport$b.prototype.perform = function perform () {
    if (!this.state.meta) { return }
    if (!this.state.meta[this.name]) { return }
    if (!this.state.meta[this.name].data) { return }

    return get(this.state, this.state.meta[this.name].data)
  };

  var defaultExport$c = function defaultExport (ref) {
    var state = ref.state;
    var name = ref.name;

    this.state = state;
    this.name = name;
  };
  defaultExport$c.prototype.perform = function perform () {
    if (!this.state.meta) { return }
    if (!this.state.meta[this.name]) { return }

    return this.state.meta[this.name].info
  };

  var defaultExport$d = function defaultExport (ref) {
    var state = ref.state;
    var name = ref.name;

    this.state = state;
    this.name = name;
  };
  defaultExport$d.prototype.perform = function perform () {
      var this$1 = this;

    if (!this.state.meta) { return }
    if (!this.state.meta[this.name]) { return }
    if (!this.state.meta[this.name].data) { return }

    return this.state.meta[this.name].data
      .map(function (entryRef) { return get(this$1.state, entryRef); })
      .filter(function (entry) { return entry; })
  };

  var defaultExport$e = function defaultExport (ref) {
    var state = ref.state;
    var entry = ref.entry;
    var name = ref.name;

    this.state = state;
    this.entry = entry;
    this.name = name;
  };
  defaultExport$e.prototype.perform = function perform () {
      var this$1 = this;

    if (!this.entry) { return }
    if (!this.entry.relationships) { return }
    if (!this.entry.relationships[this.name]) { return }
    if (!this.entry.relationships[this.name].data) { return }

    return this.entry.relationships[this.name].data.map(function (localEntry) {
      return get(this$1.state, localEntry)
    }).filter(function (entry) { return entry; })
  };

  var defaultExport$f = function defaultExport (ref) {
    var state = ref.state;
    var entry = ref.entry;
    var name = ref.name;

    this.state = state;
    this.entry = entry;
    this.name = name;
  };
  defaultExport$f.prototype.perform = function perform () {
    if (!this.entry) { return }
    if (!this.entry.relationships) { return }
    if (!this.entry.relationships[this.name]) { return }
    if (!this.entry.relationships[this.name].data) { return }

    var associated = this.entry.relationships[this.name].data;
    if (!this.entry.relationships || !this.entry.relationships[this.name]) { return }
    return get(this.state, associated)
  };

  var Getters = {
    entry: function entry (state) {
      return function (ref) { return get(state, ref); }
    },
    collection: function collection (state) {
      return function (type) { return getCollection(state, type); }
    },
    relationship: function relationship$$1 (state, getters) {
      return function (params, secondParam) {
        var entry = null;
        var name = null;

        if (params.entry) {
          entry = params.entry; 
          name = params.name;
        } else {
          entry = params;
          name = secondParam;
        }
        if (!entry.relationships || !entry.relationships[name] ||
            !entry.relationships[name].data) {
          return null
        }

        if (Array.isArray(entry.relationships[name].data)) {
          return getters.associatedEntries({ entry: entry, name: name })
        } else {
          return getters.associatedEntry({ entry: entry, name: name })
        }
      }
    },
    metaEntry: function metaEntry (state) {
      return function (name) { return new defaultExport$b({ state: state, name: name }).perform(); }
    },
    metaInfo: function metaInfo (state) {
      return function (name) { return new defaultExport$c({ state: state, name: name }).perform(); }
    },
    metaCollection: function metaCollection (state) {
      return function (name) { return new defaultExport$d({ state: state, name: name }).perform(); }
    },
    associatedEntries: function associatedEntries (state) {
      return function (ref) {
        var entry = ref.entry;
        var name = ref.name;

        return new defaultExport$e({ state: state, entry: entry, name: name }).perform();
      }
    },
    associatedEntriesLinks: function associatedEntriesLinks (state) {
      return function (ref) {
        var entry = ref.entry;
        var name = ref.name;

        return new AssociatedEntriesLinksService({ state: state, entry: entry, name: name }).perform();
      }
    },
    associatedEntry: function associatedEntry (state) {
      return function (ref) {
        var entry = ref.entry;
        var name = ref.name;

        return new defaultExport$f({ state: state, entry: entry, name: name }).perform();
      }
    },
    wasUrlCalled: function wasUrlCalled (state) {
      return function (url) {
        if (!state['called-urls']) { return false }
        return state['called-urls'][url] !== undefined
      }
    },
    urlPromise: function urlPromise (state) {
      return function (url) {
        if (!state['called-urls']) { return }
        return state['called-urls'][url]
      }
    },
    axios: function axios$1 (state) {
      return axios
    },
    endpoint: function endpoint (state) {
      return state.endpoint
    },
    errorHandler: function errorHandler (state) {
      return state.errorHandler || (function (ref) {
        var status = ref.status;
        var data = ref.data;

        if (status.toString()[0] === '5') { alert(JSON.stringify(data)); }
        if (status.toString()[0] === '4') { console.warn(state, data); }
      })
    }
  };

  Vue.use(VueResource);

  var queue = new PQueue({ concurrency: 1 });

  var required = function (params) {
    for (var i = 0, list = Object.entries(params); i < list.length; i += 1) {
      var ref = list[i];
      var paramsName = ref[0];
      var paramValue = ref[1];

      if (paramValue) { continue }
      throw new Error((paramsName + " is required"))
    }
  };

  var calculeUrl = function (ref) {
    var endpoint = ref.endpoint;
    var resource = ref.resource;
    var url = ref.url;

    if (url && endpoint && !resource && !url.includes('.')) {
      resource = url.replace(endpoint, '').replace(/^\//, '');
    }
    return (!resource && url) || (endpoint + "/" + resource)
  };

  var Actions = {
    loadRelationship: function loadRelationship (context, ref) {
      var entry = ref.entry;
      var name = ref.name;

      required({ entry: entry, name: name });

      if (!entry.relationships || !entry.relationships[name] ||
          !entry.relationships[name].data) {
        return new Promise(function (resolve) { return resolve(null); })
      }

      var relationship = entry.relationships[name];
      var result = context.getters.relationship({ entry: entry, name: name });
      
      if (Array.isArray(relationship.data)) {
        if (result.length === relationship.data.length) {
          return new Promise(function (resolve) { return resolve(result); })
        }
      } else {
        if (result) {
          return new Promise(function (resolve) { return resolve(result); })
        }
      }

      var url = relationship.links.self;

      return context.dispatch('get', { url: url }).then(function (result) { return result.data; })
    },
    add: function add (context, ref) {
      var endpoint = ref.endpoint;
      var resource = ref.resource;
      var url = ref.url;
      var cache = ref.cache; if ( cache === void 0 ) cache = true;

      url = calculeUrl({ endpoint: endpoint, resource: resource, url: url });

      return context.dispatch('request', { url: url, cache: cache }).then(function (response) {
        context.commit('addByNormalize', {
          payload: response, resource: resource
        });
        return response
      })
    },
    request: function request (context, ref) {
      var url = ref.url;
      var method = ref.method;
      var payload = ref.payload;
      var quene = ref.quene;
      var cache = ref.cache; if ( cache === void 0 ) cache = true;

      required({ url: url });

      var promise = null;
      var decoupledPayload = JSON.parse(JSON.stringify(payload || {}));
      if (method === 'get' || !method) {
        if (context.getters.wasUrlCalled(url)) {
          return context.getters.urlPromise(url)
        }
        promise = context.getters.axios.get(url, { data: decoupledPayload }).then(
          function (response) {
            return response.data
          },
          function (response) {
            var result = { status: response.response.status, data: response.response.data };

            context.getters.errorHandler(result);
            return Promise.reject(result)
          });
        if (cache) {
          context.commit('addCalledUrl', { url: url, promise: promise });
        }
        return promise
      } else {
        context.commit('clearCalledUrls');
        return queue.add(function () {
          return context.getters.axios[method](url, { data: decoupledPayload }).then(
            function (response) {
              return response
            },
            function (response) {
              var result = { status: response.response.status, data: response.response.data };

              context.getters.errorHandler(result);
              return Promise.reject(result)
            })
        })
      }
    },
    get: function get (context, param) {
      var url = null;
      var endpoint = null;
      var resource = null;
      var cache = null;

      if (typeof param == 'string') {
        url = param;
      } else {
        url = param.url;
        endpoint = param.endpoint;
        resource = param.resource;
        cache = param.cache;
      }

      if (cache === null || cache === undefined) {
        cache = true;
      }

      url || required({ resource: resource });
      if (!url && !context.getters.endpoint) { required({ endpoint: endpoint }); }
      url = calculeUrl({
        endpoint: endpoint || context.getters.endpoint,
        resource: resource,
        url: url
      });

      return context.dispatch('add', { url: url, cache: cache })
    },
    destroy: function destroy (context, params) {
      var url = null;

      if (typeof params === 'string') {
        url = calculeUrl({
          endpoint: context.getters.endpoint,
          url: params
        });
      } else {
        var entry = params.entry;
        var endpoint = params.endpoint;
        var resource = params.resource;
        url = params.url;

        if (params.id && params.type) { entry = params; }

        required({ entry: entry });

        if (!url && !context.getters.endpoint && !(entry.links || {}).self) { required({ endpoint: endpoint }); }

        url = calculeUrl({
          endpoint: endpoint || context.getters.endpoint,
          resource: resource,
          url: url || (entry.links || {}).self
        });

        context.commit('remove', entry);
      }
      return context.dispatch('request', { url: url, method: 'delete' })
    },
    update: function update (context, ref) {
      var entry = ref.entry;
      var payload = ref.payload;
      var endpoint = ref.endpoint;
      var resource = ref.resource;
      var url = ref.url;
      var delayed = ref.delayed;

      required({ entry: entry, payload: payload });
      if (!url && !(entry.links && entry.links.self)) {
        required({ resource: resource });
        if (!context.getters.endpoint) { required({ endpoint: endpoint }); }
      }

      url = calculeUrl({
        endpoint: endpoint || context.getters.endpoint,
        resource: resource,
        url: url || entry.links.self
      });

      if (!delayed) { context.commit('update', { entry: entry, payload: payload }); }

      return context
        .dispatch('request', { url: url, method: 'patch', payload: payload })
        .then(function (response) {
          if (delayed) { context.commit('update', { entry: entry, payload: payload }); }
          if (response.data.data) { context.commit('update', { entry: entry, payload: response.data.data }); }
          return response
        })
    },
    create: function create (context, ref) {
      var payload = ref.payload;
      var endpoint = ref.endpoint;
      var resource = ref.resource;
      var url = ref.url;

      endpoint = endpoint || context.getters.endpoint;
      payload = payload || {};
      url || required({ payload: payload, resource: resource });
      if (!url || !context.getters.endpoint) { required({ endpoint: endpoint }); }

      url = calculeUrl({
        endpoint: endpoint || context.getters.endpoint,
        resource: resource,
        url: url
      });

      return context.dispatch('request', { url: url, method: 'post', payload: payload })
        .then(function (response) {
          context.commit('add', response.data.data);
          return response
        })
    },
    changeOneToManyReference: function changeOneToManyReference (context, ref) {
      var child = ref.child;
      var parent = ref.parent;
      var parentRelationshipName = ref.parentRelationshipName;
      var childRelationshipName = ref.childRelationshipName;

      required({ child: child, parent: parent, parentRelationshipName: parentRelationshipName, childRelationshipName: childRelationshipName });

      context.dispatch('changeRelationship', {
        child: child,
        parent: parent,
        parentRelationshipName: parentRelationshipName,
        childRelationshipName: childRelationshipName,
        url: child.links.self,
        data: { id: parent.id, type: parent.type }
      });
    },
    changeManyToOneReference: function changeManyToOneReference (context, ref) {
      var obj;

      var children = ref.children;
      var parent = ref.parent;
      var parentRelationshipName = ref.parentRelationshipName;
      var childRelationshipName = ref.childRelationshipName;
      var endpoint = ref.endpoint;
      var parentTypes = ref.parentTypes;
      required({ children: children, parent: parent, parentRelationshipName: parentRelationshipName, childRelationshipName: childRelationshipName, endpoint: endpoint });

      context.commit('changeManyToOneReference', {
        children: children, parent: parent, parentTypes: parentTypes, parentRelationshipName: parentRelationshipName, childRelationshipName: childRelationshipName
      });
      return context.dispatch('update', {
        entry: parent,
        endpoint: endpoint,
        payload: {
          id: parent.id,
          type: parent.type,
          relationships: ( obj = {}, obj[parentRelationshipName] = { data: children }, obj )
        }
      })
    },
    changeRelationship: function changeRelationship (context, ref) {
      var obj;

      var url = ref.url;
      var child = ref.child;
      var parent = ref.parent;
      var parentRelationshipName = ref.parentRelationshipName;
      var childRelationshipName = ref.childRelationshipName;
      var data = ref.data;
      return context.dispatch('request', {
        url: url,
        method: 'put',
        payload: { relationships: ( obj = {}, obj[childRelationshipName] = { data: data }, obj ) }
      }).then(function (response) {
        context.commit('removeFromAll', {
          child: child,
          parentType: parent.type,
          parentRelationshipName: parentRelationshipName
        });
        context.commit('addToMultiple', {
          parent: parent, child: child, relationshipName: parentRelationshipName });
        context.commit('setAssociation', {
          parent: parent, child: child, relationshipName: childRelationshipName
        });
        return undefined
      })
    },
    loadAssociationNextPageAccumulative: function loadAssociationNextPageAccumulative (context, ref) {
      var entry = ref.entry;
      var association = ref.association;

      return context.dispatch('add', { url: entry.relationships[association].links.next }).then(function (response) {
        context.commit('addPageToAssociation', { entry: entry, association: association, response: response });
      })
    },
    postRelationship: function postRelationship (context, ref) {
      var entry = ref.entry;
      var relationship = ref.relationship;
      var resource = ref.resource;
      var payload = ref.payload;
      var url = ref.url;
      var endpoint = ref.endpoint;
      var delayed = ref.delayed;

      required({ entry: entry, relationship: relationship, payload: payload });
      if (!url && (
        entry.relationships &&
        entry.relationships[relationship] &&
        entry.relationships[relationship].links &&
        entry.relationships[relationship].links.self
      )) {
        url = entry.relationships[relationship].links.self;
      }

      url = calculeUrl({
        endpoint: endpoint || context.getters.endpoint,
        resource: resource,
        url: url
      });

      if (!delayed) {
        context.commit('relationship', {
          entry: entry,
          relationship: relationship,
          data: payload.data
        });
      }

      return context
        .dispatch('request', { url: url, method: 'post', payload: payload })
        .then(function (response) {
          if (delayed) {
            context.commit('relationship', {
              entry: entry,
              relationship: relationship,
              data: payload.data
            });
          }
          if (response.data.data) {
            context.commit('update', { entry: entry, payload: response.data.data });
          }
        })
    },
    createOrUpdate: function createOrUpdate (context, ref) {
      var entry = ref.entry;
      var payload = ref.payload;
      var endpoint = ref.endpoint;
      var resource = ref.resource;
      var url = ref.url;

      if (entry && entry.id) {
        return context.dispatch('update', { entry: entry, payload: payload })
      }
      else {
        return context.dispatch('create', { payload: payload, endpoint: endpoint, resource: resource, url: url })
      }
    }
  };

  var index = {
    mutations: Mutations,
    getters: Getters,
    actions: Actions
  };

  exports.Utils = utils;
  exports.default = index;

  Object.defineProperty(exports, '__esModule', { value: true });

})));

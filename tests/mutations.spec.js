import Vue from 'vue'
import VueResource from 'vue-resource'

import Mutations from '../src/json_api/mutations'

Vue.use(VueResource)

/* eslint-disable no-undef */

describe('Mutations', () => {
  describe('.clearCalledUrls', () => {
    it('remove called-urls from state', () => {
      let state = { 'called-urls': {}, vue: Vue }
      Mutations.clearCalledUrls(state)
      expect(state['called-urls']).to.be.eq(undefined)
    })
  })
})

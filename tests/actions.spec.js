import Vue from 'vue'
import VueResource from 'vue-resource'

import Actions from '../src/json_api/actions'

import sinon from 'sinon'
import sinonStubPromise from 'sinon-stub-promise'
import moxios from 'moxios'
import axios from 'axios';

/* eslint-disable no-undef */

Vue.use(VueResource)
sinonStubPromise(sinon)

describe('Actions', () => {
  def('user_url', () => '/api/v1/users/1')
  def('users_url', () => '/api/v1/users')
  def('user', () => ({
    id: '1',
    type: 'users',
    relationships: { 
      issues: { 
        data: [$issue],
        links: {
          self: '/api/v1/users/1/issues' 
        }
      }
    },
    links: { self: $user_url }
  }))
  def('issue', () => ({ id: '1', type: 'issue' }))
  def('userResponse', () => ({
    data: $user,
    included: [ $issue ]
  }))
  def('usersResponse', () => ({
    data: [ $user ],
    included: [ $issue ]
  }))
  def('wasUrlCalled', () => () => false)
  def('urlPromise', () => { return sinon.stub().resolves($user) })
  def('getters', () => ({
    get: () => $user,
    relationship: () => [],
    endpoint: '/api/v1',
    wasUrlCalled: $wasUrlCalled,
    urlPromise: $urlPromise,
    axios: axios,
    errorHandler: () => null
  }))
  def('commit', () => sinon.spy())
  def('dispatch', () => sinon.spy())
  def('context', () => ({
    getters: $getters,
    commit: $commit,
    dispatch: $dispatch
  }))

  describe('#loadRelationship', () => {
    def('dispatch', () => sinon.stub().resolves({ data: [$issue] }) )

    it('call Actions#get', () => {
      Actions.loadRelationship($context, { entry: $user, name: 'issues' })
      expect($context.dispatch).to.have.been.calledWith('get', {
        url: '/api/v1/users/1/issues' 
      })
    })
  })

  describe('#get', () => {
    describe('when the url has not be called before', () => {
      def('dispatch', () => sinon.stub().resolves({ data: $issue }) )

      it('call Actions#add', () => {
        Actions.get($context, { resource: 'users' })
        expect($context.dispatch).to.have.been.calledWith('add', {
          url: '/api/v1/users',
          cache: true
        })
      })

      describe('when url is passed as parameter', () => {
        it('call Actions.add', () => {
          Actions.get($context, { url: 'www.example.com/users' })
          expect($context.dispatch).to.have.been.calledWith('add', {
            url: 'www.example.com/users',
            cache: true
          })
        })
      })
      describe('when url and cache is passed as parameter', () => {
        it('call Actions.add', () => {
          Actions.get($context, { url: 'www.example.com/users', cache: false })
          expect($context.dispatch).to.have.been.calledWith('add', {
            url: 'www.example.com/users',
            cache: false
          })
        })
      })
      describe('when the url is passed directly as parameter', () => {
        it('call Actions.add', () => {
          Actions.get($context, 'www.example.com/users')
          expect($context.dispatch).to.have.been.calledWith('add', {
            url: 'www.example.com/users', 
            cache: true
          })
        })
      })
    })
  })
  describe('#create', () => {
    def('dispatch', () => sinon.stub().resolves({ data: { data: $issue } }) )

    it('call Actions#request', () => {
      Actions.create($context, { resource: 'users', payload: $user })
      expect($context.dispatch).to.have.been.calledWith('request', {
        url: '/api/v1/users', method: 'post', payload: $user })
    })

    describe('when url is passed as parameter', () => {
      it('call Actions#request', () => {
        Actions.create($context, { entry: $user, payload: $user, url: 'www.example.com/users' })
        expect($context.dispatch).to.have.been.calledWith('request', {
          url: 'www.example.com/users',
          method: 'post',
          payload: {
            id: '1',
            type: 'users',
            links: { self: '/api/v1/users/1' },
            relationships: {
              issues: {
                data: [{ id: "1", type: "issue" }],
                links: { self: "/api/v1/users/1/issues" }
              }
            },
          },
          url: "www.example.com/users"
        })
      })
    })
    describe('when payload is not passed as parameter', () => {
      it('call Actions#request', () => {
        Actions.create($context, { entry: $user, url: 'www.example.com/users' })
        expect($context.dispatch).to.have.been.calledWith('request', {
          method: 'post',
          url: 'www.example.com/users',
          payload: {} 
        })
      })
    })
  }),
  describe('#destroy', () => {
    def('dispatch', () => sinon.stub().resolves({ data: $issue }) )

    it('call Mutations#remove', () => {
      Actions.destroy($context, { entry: $user })
      expect($context.commit).to.have.been.calledWith('remove', $user)
    })

    it('call Mutations#remove when params=entry', () => {
      Actions.destroy($context, $user)
      expect($context.commit).to.have.been.calledWith('remove', $user)
    })

    it('call Actions#request', () => {
      Actions.destroy($context, { entry: $user })
      expect($context.dispatch).to.have.been.calledWith('request', {
        method: 'delete',
        url: '/api/v1/users/1' 
      })
    })

    it('call Actions#request when url is passed as paramter', () => {
      Actions.destroy($context, { entry: $user, url: 'www.example.com/explicit/users/1' })
      expect($context.dispatch).to.have.been.calledWith('request', {
        method: 'delete',
        url: 'www.example.com/explicit/users/1'
      })
    })

    describe('when resource is passed as paramter', () => {
      it('call Actions#request', () => {
        Actions.destroy($context, { entry: $user, resource: 'users/2' })
        expect($context.dispatch).to.have.been.calledWith('request', {
          method: 'delete',
          url: '/api/v1/users/2'
        })
      })
    })

    it('call Actions#request when the paramter is a string', () => {
      Actions.destroy($context, '/api/v1/users/2')
      expect($context.dispatch).to.have.been.calledWith('request', {
        method: 'delete',
        url: '/api/v1/users/2'
      })
    })
  }),
  describe('#update', () => {
    def('dispatch', () => sinon.stub().resolves({ data: $issue }) )

    it('call Commit#update', () => {
      Actions.update($context, { 
        entry: $user, 
        payload: { attributes: { name: 'test' } }
      })
      expect($context.commit).to.have.been.calledWith('update', {
        entry: $user,
        payload: { attributes: { name: 'test' } }
      })
    })

    it('call Actions#request', () => {
      Actions.update($context, { 
        entry: $user, 
        payload: { attributes: { name: 'test' } }
      })
      expect($context.dispatch).to.have.been.calledWith('request', {
        method: 'patch',
        payload: { attributes: { name: "test" } },
        url: '/api/v1/users/1' 
      })
    })

    describe('when url is passed as paramter', () => {
      it('call Actions#request', () => {
        Actions.destroy($context, { entry: $user, url: 'www.example.com/explicit/users/1' })
        expect($context.dispatch).to.have.been.calledWith('request', {
          method: 'delete',
          url: 'www.example.com/explicit/users/1'
        })
      })
    })

    describe('when resource is passed as paramter', () => {
      it('call Actions#request', () => {
        Actions.destroy($context, { entry: $user, resource: 'users/2' })
        expect($context.dispatch).to.have.been.calledWith('request', {
          method: 'delete',
          url: '/api/v1/users/2'
        })
      })
    })
  }),
  describe('#add', () => {
    def('request', () => sinon.stub().resolves($userResponse))
    def('dispatch', () => {
      return sinon.stub().withArgs('request', { url: $user_url, cache: true })
        .returns($request())
    })

    it('call commit .addByNormalize and .request', (done) => {
      Actions.add($context, { endpoint: 'api/v1', resource: 'users' }).then(() => {
        expect($context.commit).to.have.been.calledWith('addByNormalize', {
          payload: $userResponse, resource: 'users'
        })
        expect($dispatch).to.have.been.calledWith('request', {
          url: 'api/v1/users',
          cache: true
        })
	      done()
      })
    })
  })
  describe('#request', (done) => {
    def('server', () => sinon.fakeServer.create())
    beforeEach(() => { moxios.install() })
    afterEach(() => { moxios.uninstall() })

    it('return a a promise from the http request', (done) => {
      Actions.request($context, { url: $user_url }).then(response => {
        expect(response).to.eql($userResponse)
        done()
      })

      moxios.wait(() => {
        moxios.requests.mostRecent().respondWith({
          status: 200,
          response: JSON.stringify($userResponse)
        })
      })
    })

    it('call addCalledUrl when the method is get', (done) => {
      Actions.request($context, { url: $user_url }).then(response => {
        expect($context.commit).to.have.been.calledWith('addCalledUrl')
        done()
      })

      moxios.wait(() => {
        moxios.requests.mostRecent().respondWith({
          status: 200,
          response: JSON.stringify($userResponse)
        })
      })
    })

    it('dies not call addCalledUrl when the method is get and cache === false', (done) => {
      Actions.request($context, { url: $user_url, cache: false }).then(response => {
        expect($context.commit).not.to.have.been.calledWith('addCalledUrl')
        done()
      })

      moxios.wait(() => {
        moxios.requests.mostRecent().respondWith({
          status: 200,
          response: JSON.stringify($userResponse)
        })
      })
    })

    it('does not call addCalledUrl when the method is not get', (done) => {
      Actions.request($context, { url: $user_url, method: 'post' }).then(response => {
        expect($context.commit).not.to.have.been.calledWith('addCalledUrl')
        done()
      })

      moxios.wait(() => {
        moxios.requests.mostRecent().respondWith({
          status: 200,
          response: JSON.stringify($userResponse)
        })
      })
    })

    it('call clearCalledUrls when the method is not get', (done) => {
      Actions.request($context, { url: $user_url, method: 'post' }).then(response => {
        expect($context.commit).to.have.been.calledWith('clearCalledUrls')
        done()
      })

      moxios.wait(() => {
        moxios.requests.mostRecent().respondWith({
          status: 200,
          response: JSON.stringify($userResponse)
        })
      })
    })

    describe('when the promise was cached', () => {
      def('wasUrlCalled', () => () => true)
      
      it('do not make http request when the promise was cached', (done) => {
        Actions.request($context, { url: $user_url }).then(response => {
          done()
        })
      })
    })

    it('call getters.errorHandler({ status, data })', (done) => {
      let result = { errors: ['whatever'] }
      $getters.errorHandler = ({ status, data }) => {
        expect(status).to.eql(400)
        expect(data).to.eql(result)
        done()
      }

      Actions.request($context, { url: $user_url }).catch(() => null)

      moxios.wait(() => {
        moxios.requests.mostRecent().respondWith({
          status: 400,
          response: JSON.stringify(result)
        })
      })
    })
  })
  describe('#loadAssociationNextPageAccumulative', () => {
    def('response', () => ({
      links: { next: '/api/v1/page/1/users?page=3' },
      data: [{ id: 1, type: 'users' }]
    }))
    def('entry', () => ({
      data: {
        id: 1,
        type: 'pages'
      },
      relationships: {
        users: {
          data: [{ id: 1, type: 'users' }],
          links: { next: '/api/v1/page/1/users?page=2' }
        }
      }
    }))
    def('dispatch', () => sinon.stub().withArgs('add', { url: '/api/v1/page/1/users?page=2' }).resolves($response))

    it('call Actions#add', () => {
      Actions.loadAssociationNextPageAccumulative($context, { entry: $entry, association: 'users' })
      expect($dispatch).to.have.been.calledWith('add', { url: '/api/v1/page/1/users?page=2' })
    })
    it('call Mutations#addPageToAssociation', (done) => {
      Actions.loadAssociationNextPageAccumulative($context, { entry: $entry, association: 'users' }).then(() => {
        expect($commit).to.have.been.calledWith('addPageToAssociation', { entry: $entry, association: 'users', response: $response })
        done()
      })
    })
  })
  describe('#postRelationship', () => {
    def('dispatch', () => sinon.stub().resolves({ data: { data: null } }) )
    def('payload', () => ({ data: [{ id: '1', type: 'issues' }] }))

    it('call Actions#request', () => {
      Actions.postRelationship($context, {
        entry: $user,
        relationship: 'issues',
        payload: $payload
      })
      expect($context.dispatch).to.have.been.calledWith('request', {
        url: '/api/v1/users/1/issues', method: 'post', payload: $payload })
    })
  })
  describe('.createOrUpdate', () => {
    def('dispatch', () => sinon.stub() )
    def('payload', () => ({ data: [{ id: '1', type: 'issues' }] }))

    it('call Actions.create when entry is null', () => {
      Actions.createOrUpdate($context, {
        entry: null,
        resource: 'issues',
        payload: $payload
      })
      expect($context.dispatch).to.have.been.calledWith('create', {
        endpoint: undefined,
        resource: 'issues',
        payload: $payload,
        url: undefined
      })
    })
    it('call Actions.update when entry is present', () => {
      Actions.createOrUpdate($context, {
        resource: 'issues',
        entry: $issue,
        payload: $payload
      })
      expect($context.dispatch).to.have.been.calledWith('update', {
        entry: $issue,
        payload: $payload
      })
    })
  })
})

import Vue from 'vue'
import VueResource from 'vue-resource'

import Getters from '../src/json_api/getters'

Vue.use(VueResource)

/* eslint-disable no-undef */

describe('Getters', () => {
  def('user', () => ({
    id: '1',
    type: 'users',
    relationships: {
      projects: { data: [{ id: '1', type: 'projects' }] }
    }
  }))
  def('project', () => ({
    id: '1',
    type: 'projects'
  }))
  def('state', () => ({
    vue: Vue,
    users: { 1: $user },
    projects: { 1: $project }, 
    meta: {
      users: { data: [{ id: '1', type: 'users' }] }
    }
  }))

  describe('#entry', () => {
    describe('with the expected entry', () => {
      it('return the expected entry', () => {
        expect(Getters.entry($state)({ type: 'users', id: '1' })).to.eql($user)
      })
    })

    describe('without the expected entry', () => {
      def('state', () => ({ }))

      it('return the expected entry', () => {
        expect(Getters.entry($state)({ type: 'users', id: '1' })).to.eql(undefined)
      })
    })

    describe('when the param is null', () => {
      def('state', () => ({ }))

      it('return undefined', () => {
        expect(Getters.entry($state)(null)).to.eql(undefined)
      })
    })
  })
  describe('#collection', () => {
    describe('with the coresponding collection', () => {
      it('return the expected collection', () => {
        expect(Getters.collection($state)('users')).to.eql([$user])
      })
    })
    describe('without the coresponding collection', () => {
      def('state', () => ({ }))

      it('return undefined', () => {
        expect(Getters.collection($state)('users')).to.eql(undefined)
      })
    })
  })
  describe('#relationship', () => {
    def('getters', () => {
      return {
        associatedEntries: () => [$project]
      }
    })

    it('return the relationship when passing as object', () => {
      expect(Getters.relationship($state, $getters)({ entry: $user, name: 'projects' })).to.eql([$project])
    })
    it('return the relationship when passing as args', () => {
      expect(Getters.relationship($state, $getters)($user, 'projects')).to.eql([$project])
    })
  })
})

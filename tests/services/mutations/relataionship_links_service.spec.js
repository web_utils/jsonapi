import Vue from 'vue'
import VueResource from 'vue-resource'

import RelataionshipLinksService from '../../../src/json_api/services/mutations/relataionship_links_service'

Vue.use(VueResource)

/* eslint-disable no-undef */

describe('RelataionshipLinksService', () => {
  def('entry', () => ({
    data: { id: 1, type: 'pages' },
    relationships: { users: { data: [$user1] } }
  }))
  def('links', () => ({
    next: '/api/v1/pages/1/users?page=2'
  }))
  beforeEach(() => { 
    new RelataionshipLinksService({ 
      vue: Vue,
      entry: $entry,
      association: 'users',
      links: $links 
    }).perform()
  })

  it('set links', () => {
    expect($entry.relationships.users.links).to.eql($links)
  })
})

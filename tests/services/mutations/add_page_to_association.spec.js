import Vue from 'vue'
import VueResource from 'vue-resource'

import AddPageToAssociationService from '../../../src/json_api/services/mutations/add_page_to_association_service'

Vue.use(VueResource)

/* eslint-disable no-undef */

describe('AddPageToAssociationService', () => {
  def('user1', () => ({
    data: { id: 1, type: 'users' }
  }))
  def('user2', () => ({
    data: { id: 2, type: 'users' }
  }))
  def('entry', () => ({
    data: { id: 1, type: 'pages' },
    relationships: {
      users: {
        data: [$user1],
        links: { next: '/api/v1/page/1/users?page=2' }
      }
    }
  }))
  def('response', () => ({
    data: [$user2],
    links: { next: '/api/v1/page/1/users?page=3' }
  }))
  beforeEach(() => { new AddPageToAssociationService({ vue: Vue, entry: $entry, association: 'users', response: $response }).perform() })

  it('add page to association data', () => {
    expect($entry.relationships.users.data).to.eql([$user1, $user2])
  })

  it('set page on association links', () => {
    expect($entry.relationships.users.links).to.eql({ next: '/api/v1/page/1/users?page=3' })
  })
})

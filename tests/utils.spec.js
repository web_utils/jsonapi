import { entryToRef, entryArrayToRef } from '../src/json_api/utils'

/* eslint-disable no-undef */

describe('Utils', () => {
  describe('entryToRef', () => {
    it('return ref', () => {
      const entry = { id: 1, type: 'projects', attributes: { test: 'test' } }

      expect(entryToRef(entry)).to.be.eql({ id: 1, type: 'projects' })
    })

    it('return null when parameter is null', () => {
      expect(entryToRef(null)).to.be.eql(null)
    })
  })
  describe('entryArrayToRef', () => {
    it('return ref array', () => {
      const entry = { id: 1, type: 'projects', attributes: { test: 'test' } }

      expect(entryArrayToRef([entry])).to.be.eql([{ id: 1, type: 'projects' }])
    })

    it('return null when parameter is null', () => {
      expect(entryArrayToRef(undefined)).to.be.eql(undefined)
    })
  })
})

A rest client that follow the [jsonapi specificaccion](https://jsonapi.org/)

### Status (alpha)
The library was created by refactoring the CMC project. The refactoring
is still in progress. 

### Actions
* `get(url)`: Makes a get ajax request. The entries returned by the request are stored in vuex.

  NOTICE: The ajax request/response is cached. To avoid that the response is cached, use `get({ url, cache: false })`
  ```js
  import { Utils } from 'vuex-jsonapi-client'
  ...
  data () {
    return {
      ticketRefs = []
    }
  }
  async mounted () {
    response = await this.$store.dispatch('get', 'www.example.com/api/tickets')
    ticketRefs = Utils.entryArrayToRef(response.data)
  }
  computed: {
    tickets () {
      return ticketRefs.map(ref => {
        return this.$store.entry(ref)
      })
    }
  }
  ```
* `create({ resource, payload })`: Ajax POST request. The result is stored in vuex.
  ```js
  this.$store.create({
    resource: 'www.example.com/api/v1/users', // or just 'users' when endpoint is set
    payload: {
      attributes: {
        firstname: 'Lara'
      }
    }
  })
  ```
* `update({ entry, payload })`: Ajax PUT call. It merge, the passed payload,
with the entry and stored it to vuex, before making the ajax request.
  ```js
  const user = {
    id: '1',
    type: 'users',
    links: {
      self: '/api/v1/users/1'
    }
  }

  this.$store.update({
    entry: user,
    payload: {
      attributes: {
        firstname: 'Lara'
      }
    }
  })
  ```
* `createOrUpdate({ resource, entry, payload })`: Create or update the resource depending
if it exists or not. If the entry is not null, then it requires that the entry has the field: `links.self`
  ```js
  this.$store.createOrUpdate({
    resource: 'www.example.com/api/v1/users', // or just 'users' when endpoint is set
    entry: this.$store.getters.entry({ id: '1', 'users' }),
    payload: {
      attributes: {
        firstname: 'Lara'
      }
    }
  })
  ```
* `destroy({ entry })`: Ajax DELETE request. It remove the entry from vuex before making the request.
  ```js
  const user = {
    id: '1',
    type: 'users',
    links: {
      self: '/api/v1/users/1'
    }
  }
  this.$store.destroy(user)
  ```
  ```js
  this.$store.destroy('/api/v1/users/1')
  ```
* `loadRelationship({ entry, name })`: check if all entries in a relationship are
  ```js
  const user = {
    id: '1',
    type: 'users',
    relationships: {
      projects: {
        ...
        links: {
          self: '/api/v1/users/1/projects'
        }
      }
    }
  }
  this.$store.loadRelationship({
    entry: user,
    name: 'projects'
  })
  ```
  loaded, if this is the case, then it return a promise with the entries, if not nall entries could be founded, then it call the api, by the url indicated in the relationship.
* `request({ method = get, payload, url })`: perform a request. Default request type is GET.
* `postRelationship({ entry, relationship, payload, delayed = false })`: replace one relationship
  ```js
  computed: {
    ...
    labels: {
      set (labels) {
        this.$store.disptach('postRelationship', {
          entry: this.ticket,
          relationship: 'labels',
          payload: { data: labels }
        })
      },
      get () {
        ...
      }
    }
  }
  ```

### Getters
* `entry({ entry, id })`: return an entry.
  ```js
    props: {
      entry: {
        id: { required: true },
        type: { required: true }
      }
    }
    computed: {
      ticket () {
        return this.$store.getters(this.ticketRef)
      }
    }
  ```
* `relationship(entry, name)`: return the, loaded, entry/entries of an relationship.
  ```js
    computed: {
      ticket () {
        ...
      },
      labels () {
        return this.$store.getters.relationsip(this.ticket, 'labels')
      }
    }
  ```
### Utils

* `entryToRef(entry)`
  ```js
  import { Utils } from 'vuex-jsonapi-client'

  const entry = { id: 1, type: 'labels', attributes: { name: 'test' } }

  // prints { id: 1, type: 'labels' }
  console.log(Utils.entryToRef(entry))
  ```
* `entryArrayToRef(entryArray)`
  ```js
  import { Utils } from 'vuex-jsonapi-client'

  const entryArray = [
    { id: 1, type: 'labels', attributes: { name: 'test1' } },
    { id: 2, type: 'labels', attributes: { name: 'test1' } }
  ]

  // prints [{ id: 1, type: 'labels' }, { id: 2, type: 'labels' }]
  console.log(Utils.entryArrayToRef(entryArry))
  ```

### History

Links:
- Created by refactoring: https://gitlab.com/cmc_system/cmc

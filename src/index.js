import Mutations from './json_api/mutations'
import Getters from './json_api/getters'
import Actions from './json_api/actions'
import * as Utils from './json_api/utils'

export { Utils }

export default {
  mutations: Mutations,
  getters: Getters,
  actions: Actions
}

export const get = (state, ref) => {
  if (!ref) return
  if (!state[ref.type]) return
  return state[ref.type][ref.id]
}

export const getCollection = (state, type) => {
  if (!state[type]) return
  return Object.values(state[type])
}

export const entryToRef = (entry) => {
  if (!entry) return entry
  return { id: entry.id, type: entry.type }
}

export const sameRef = (entry1, entry2) => {
  return entry1 && entry2 &&
    entry1.type === entry2.type &&
    entry1.id === entry2.id
}

export const entryArrayToRef = (entryArray) => {
  if (!entryArray) return entryArray
  return entryArray.map(entry => entryToRef(entry))
}

export const attribute = (entry, name) => {
  if (!entry) return
  if (!entry.attributes) return
  return entry.attributes[name]
}

export const relationship = (entry, name) => {
  if (!entry) return
  if (!entry.relationships) return
  if (!entry.relationships[name]) return
  return entry.relationships[name].data
}

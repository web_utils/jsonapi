import Vue from 'vue'

import VueResource from 'vue-resource'
import PQueue from 'p-queue'

Vue.use(VueResource)

const queue = new PQueue({ concurrency: 1 })

const required = (params) => {
  for (const [paramsName, paramValue] of Object.entries(params)) {
    if (paramValue) continue
    throw new Error(`${paramsName} is required`)
  }
}

const calculeUrl = ({ endpoint, resource, url }) => {
  if (url && endpoint && !resource && !url.includes('.')) {
    resource = url.replace(endpoint, '').replace(/^\//, '')
  }
  return (!resource && url) || `${endpoint}/${resource}`
}

export default {
  loadRelationship (context, { entry, name }) {
    required({ entry, name })

    if (!entry.relationships || !entry.relationships[name] ||
        !entry.relationships[name].data) {
      return new Promise(resolve => resolve(null))
    }

    const relationship = entry.relationships[name]
    const result = context.getters.relationship({ entry, name })
    
    if (Array.isArray(relationship.data)) {
      if (result.length === relationship.data.length) {
        return new Promise(resolve => resolve(result))
      }
    } else {
      if (result) {
        return new Promise(resolve => resolve(result))
      }
    }

    const url = relationship.links.self

    return context.dispatch('get', { url }).then(result => result.data)
  },
  add (context, { endpoint, resource, url, cache = true }) {
    url = calculeUrl({ endpoint, resource, url })

    return context.dispatch('request', { url, cache }).then(response => {
      context.commit('addByNormalize', {
        payload: response, resource
      })
      return response
    })
  },
  request (context, { url, method, payload, quene, cache = true }) {
    required({ url })

    let promise = null
    let decoupledPayload = JSON.parse(JSON.stringify(payload || {}))
    if (method === 'get' || !method) {
      if (context.getters.wasUrlCalled(url)) {
        return context.getters.urlPromise(url)
      }
      promise = context.getters.axios.get(url, { data: decoupledPayload }).then(
        response => {
          return response.data
        },
        response => {
          let result = { status: response.response.status, data: response.response.data }

          context.getters.errorHandler(result)
          return Promise.reject(result)
        })
      if (cache) {
        context.commit('addCalledUrl', { url, promise })
      }
      return promise
    } else {
      context.commit('clearCalledUrls')
      return queue.add(() => {
        return context.getters.axios[method](url, { data: decoupledPayload }).then(
          response => {
            return response
          },
          response => {
            let result = { status: response.response.status, data: response.response.data }

            context.getters.errorHandler(result)
            return Promise.reject(result)
          })
      })
    }
  },
  get (context, param) {
    let url = null
    let endpoint = null
    let resource = null
    let cache = null

    if (typeof param == 'string') {
      url = param
    } else {
      url = param.url
      endpoint = param.endpoint
      resource = param.resource
      cache = param.cache
    }

    if (cache === null || cache === undefined) {
      cache = true
    }

    url || required({ resource })
    if (!url && !context.getters.endpoint) required({ endpoint })
    url = calculeUrl({
      endpoint: endpoint || context.getters.endpoint,
      resource,
      url
    })

    return context.dispatch('add', { url, cache })
  },
  destroy (context, params) {
    let url = null

    if (typeof params === 'string') {
      url = calculeUrl({
        endpoint: context.getters.endpoint,
        url: params
      })
    } else {
      let { entry, endpoint, resource } = params
      url = params.url

      if (params.id && params.type) entry = params

      required({ entry })

      if (!url && !context.getters.endpoint && !(entry.links || {}).self) required({ endpoint })

      url = calculeUrl({
        endpoint: endpoint || context.getters.endpoint,
        resource,
        url: url || (entry.links || {}).self
      })

      context.commit('remove', entry)
    }
    return context.dispatch('request', { url, method: 'delete' })
  },
  update (context, { entry, payload, endpoint, resource, url, delayed }) {
    required({ entry, payload })
    if (!url && !(entry.links && entry.links.self)) {
      required({ resource })
      if (!context.getters.endpoint) required({ endpoint })
    }

    url = calculeUrl({
      endpoint: endpoint || context.getters.endpoint,
      resource,
      url: url || entry.links.self
    })

    if (!delayed) context.commit('update', { entry, payload })

    return context
      .dispatch('request', { url, method: 'patch', payload })
      .then(response => {
        if (delayed) context.commit('update', { entry, payload })
        if (response.data.data) context.commit('update', { entry, payload: response.data.data })
        return response
      })
  },
  create (context, { payload, endpoint, resource, url }) {
    endpoint = endpoint || context.getters.endpoint
    payload = payload || {}
    url || required({ payload, resource })
    if (!url || !context.getters.endpoint) required({ endpoint })

    url = calculeUrl({
      endpoint: endpoint || context.getters.endpoint,
      resource,
      url
    })

    return context.dispatch('request', { url, method: 'post', payload })
      .then(response => {
        context.commit('add', response.data.data)
        return response
      })
  },
  changeOneToManyReference (context, { child, parent,
    parentRelationshipName, childRelationshipName }) {
    required({ child, parent, parentRelationshipName, childRelationshipName })

    context.dispatch('changeRelationship', {
      child,
      parent,
      parentRelationshipName,
      childRelationshipName,
      url: child.links.self,
      data: { id: parent.id, type: parent.type }
    })
  },
  changeManyToOneReference (context, { children, parent,
    parentRelationshipName, childRelationshipName, endpoint, parentTypes }) {
    required({ children, parent, parentRelationshipName, childRelationshipName, endpoint })

    context.commit('changeManyToOneReference', {
      children, parent, parentTypes, parentRelationshipName, childRelationshipName
    })
    return context.dispatch('update', {
      entry: parent,
      endpoint,
      payload: {
        id: parent.id,
        type: parent.type,
        relationships: {
          [parentRelationshipName]: { data: children }
        }
      }
    })
  },
  changeRelationship (context, { url, child, parent,
    parentRelationshipName, childRelationshipName, data }) {
    return context.dispatch('request', {
      url,
      method: 'put',
      payload: { relationships: {
        [childRelationshipName]: { data: data }
      } }
    }).then(response => {
      context.commit('removeFromAll', {
        child: child,
        parentType: parent.type,
        parentRelationshipName: parentRelationshipName
      })
      context.commit('addToMultiple', {
        parent, child, relationshipName: parentRelationshipName })
      context.commit('setAssociation', {
        parent, child, relationshipName: childRelationshipName
      })
      return undefined
    })
  },
  loadAssociationNextPageAccumulative (context, { entry, association }) {
    return context.dispatch('add', { url: entry.relationships[association].links.next }).then(response => {
      context.commit('addPageToAssociation', { entry, association, response })
    })
  },
  postRelationship (context, { entry, relationship, resource, payload, url, endpoint, delayed }) {
    required({ entry, relationship, payload })
    if (!url && (
      entry.relationships &&
      entry.relationships[relationship] &&
      entry.relationships[relationship].links &&
      entry.relationships[relationship].links.self
    )) {
      url = entry.relationships[relationship].links.self
    }

    url = calculeUrl({
      endpoint: endpoint || context.getters.endpoint,
      resource,
      url: url
    })

    if (!delayed) {
      context.commit('relationship', {
        entry,
        relationship,
        data: payload.data
      })
    }

    return context
      .dispatch('request', { url, method: 'post', payload })
      .then(response => {
        if (delayed) {
          context.commit('relationship', {
            entry,
            relationship,
            data: payload.data
          })
        }
        if (response.data.data) {
          context.commit('update', { entry, payload: response.data.data })
        }
      })
  },
  createOrUpdate (context, { entry, payload, endpoint, resource, url }) {
    if (entry && entry.id) {
      return context.dispatch('update', { entry, payload })
    }
    else {
      return context.dispatch('create', { payload, endpoint, resource, url })
    }
  }
}

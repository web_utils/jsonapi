import * as Utils from './utils'

import MetaEntryService from './services/getters/meta_entry_service'
import MetaInfoService from './services/getters/meta_info_service'
import MetaCollectionService from './services/getters/meta_collection_service'
import AssociatedEntriesService from './services/getters/associated_entries_service'
import AssociatedEntryService from './services/getters/associated_entry_service'
import axios from 'axios'

export default {
  entry (state) {
    return (ref) => Utils.get(state, ref)
  },
  collection (state) {
    return (type) => Utils.getCollection(state, type)
  },
  relationship (state, getters) {
    return (params, secondParam) => {
      let entry = null
      let name = null

      if (params.entry) {
        entry = params.entry 
        name = params.name
      } else {
        entry = params
        name = secondParam
      }
      if (!entry.relationships || !entry.relationships[name] ||
          !entry.relationships[name].data) {
        return null
      }

      if (Array.isArray(entry.relationships[name].data)) {
        return getters.associatedEntries({ entry, name })
      } else {
        return getters.associatedEntry({ entry, name })
      }
    }
  },
  metaEntry (state) {
    return (name) => new MetaEntryService({ state, name }).perform()
  },
  metaInfo (state) {
    return (name) => new MetaInfoService({ state, name }).perform()
  },
  metaCollection (state) {
    return (name) => new MetaCollectionService({ state, name }).perform()
  },
  associatedEntries (state) {
    return ({ entry, name }) => new AssociatedEntriesService({ state, entry, name }).perform()
  },
  associatedEntriesLinks (state) {
    return ({ entry, name }) => new AssociatedEntriesLinksService({ state, entry, name }).perform()
  },
  associatedEntry (state) {
    return ({ entry, name }) => new AssociatedEntryService({ state, entry, name }).perform()
  },
  wasUrlCalled (state) {
    return (url) => {
      if (!state['called-urls']) return false
      return state['called-urls'][url] !== undefined
    }
  },
  urlPromise (state) {
    return (url) => {
      if (!state['called-urls']) return
      return state['called-urls'][url]
    }
  },
  axios (state) {
    return axios
  },
  endpoint (state) {
    return state.endpoint
  },
  errorHandler (state) {
    return state.errorHandler || (({ status, data }) => {
      if (status.toString()[0] === '5') alert(JSON.stringify(data))
      if (status.toString()[0] === '4') console.warn(state, data)
    })
  }
}

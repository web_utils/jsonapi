import * as Utils from '../../utils'

export default class {
  constructor ({ state, payload }) {
    this.state = state
    this.payload = payload
  }
  perform () {
    this._init()
    this._add()
  }

  _init () {
    if (this.state[this._type()]) return
    this._vue().set(this.state, this._type(), {})
  }
  _add () {
    if (this._isPresent()) return

    this._vue().set(this._collection(), this.payload.id, this.payload)
  }

  _type () {
    return this.payload.type
  }
  _collection () {
    return this.state[this._type()]
  }

  _isPresent () {
    return Utils.get(this.state, {
      type: this._type(),
      id: this.payload.id
    }) !== undefined
  }
  _vue () {
    return this.state.vue
  }
}

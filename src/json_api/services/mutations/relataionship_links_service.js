export default class {
  constructor ({ vue, entry, association, links }) {
    this.vue = vue
    this.entry = entry
    this.association = association
    this.links = links
  }
  perform () {
    this.vue.set(this._relationship(), 'links', this.links)
  }

  _relationship () {
    return this.entry.relationships[this.association]
  }
}

export default class {
  constructor ({ vue, entry, association, response }) {
    this.vue = vue
    this.entry = entry
    this.association = association
    this.response = response
  }
  perform () {
    this._addData()
    this._setLink()
  }

  _addData () {
    this.response.data.forEach(item => {
      this._relationship().data.push(item)
    })
  }
  _setLink () {
    this.vue.set(this._relationship(), 'links', this.response.links)
  }

  _relationship () {
    return this.entry.relationships[this.association]
  }
}

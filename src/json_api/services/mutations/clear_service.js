export default class {
  constructor ({ state, exclude }) {
    this.state = state
    this.exclude = exclude
  }
  perform () {
    for (let key of Object.keys(this.state)) {
      if (key === 'config') continue
      this._vue().delete(this.state, key)
    }
  }

  _vue () {
    return this.state.vue
  }
}
